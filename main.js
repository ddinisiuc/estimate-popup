class EstimateItem {
    constructor(
        title,
        description,
        materials = [],
        labours = [],
        price = 0,
        subtotal,
        tax_rate = 0,
        total,
        material_amount = 0,
        labours_amount = 0
    ) {
        this.title = title;
        this.description = description;
        this.materials = materials;
        this.labours = labours;
        this.price = price;
        this.subtotal = subtotal;
        this.tax_rate = tax_rate;
        this.total = total;
        this.material_amount = material_amount;
        this.labours_amount = labours_amount;
    }

    getTemplate(row){
        return " <div data-row=" + row + " class=\"pwrk-frm__section pwrk-frm__item\" data-test=\"work-item\">\n" +
            "                            <div class=\"pwrk-frm__item-desc\">\n" +
            "                                <div class=\"pwrk-frm__input-list\">\n" +
            "                                    <input name=\"title\"\n" +
            "                                           placeholder=\"Give this work a title\"\n" +
            "                                           autocomplete=\"off\" inputmode=\"\" maxlength=\"\"\n" +
            "                                           class=\"pwrk-frm__input pwrk-frm__input--title\"\n" +
            "                                           data-test=\"title-field\" id=\"title\" value=\"\">\n" +
            "                                    <div></div>\n" +
            "                                    <div class=\"pwrk-frm__block\">\n" +
            "                                        <div class=\"trix__wrapper\">\n" +
            "                                            <input type=\"hidden\"\n" +
            "                                                   id='6011882f-12ce-4f9b-8b45-3cb410c4ff67-" + row + "'\n" +
            "                                                   name=\"description\" value=\"\">\n" +
            "                                            <trix-toolbar  id='trix-toolbar-10" +row+ "'>\n" +
            "                                                <div class=\"trix-button-row\">\n" +
            "                                             <span class=\"trix-button-group trix-button-group--text-tools\" data-trix-button-group=\"text-tools\">\n" +
            "                                             <button type=\"button\" class=\"trix-button trix-button--icon trix-button--icon-bold\" data-trix-attribute=\"bold\"\n" +
            "                                                     data-trix-key=\"b\" title=\"Bold\" tabindex=\"-1\">Bold</button>\n" +
            "                                             <button type=\"button\" class=\"trix-button trix-button--icon trix-button--icon-italic\" data-trix-attribute=\"italic\"\n" +
            "                                                     data-trix-key=\"i\" title=\"Italic\" tabindex=\"-1\">Italic</button>\n" +
            "                                             <button type=\"button\" class=\"trix-button trix-button--icon trix-button--icon-strike\" data-trix-attribute=\"strike\"\n" +
            "                                                     title=\"Strikethrough\" tabindex=\"-1\">Strikethrough</button>\n" +
            "                                             <button type=\"button\" class=\"trix-button trix-button--icon trix-button--icon-link\" data-trix-attribute=\"href\"\n" +
            "                                                     data-trix-action=\"link\" data-trix-key=\"k\" title=\"Link\" tabindex=\"-1\">Link</button>\n" +
            "                                             </span>\n" +
            "                                                    <span class=\"trix-button-group trix-button-group--block-tools\"\n" +
            "                                                          data-trix-button-group=\"block-tools\">\n" +
            "                                             <button type=\"button\" class=\"trix-button trix-button--icon trix-button--icon-heading-1\"\n" +
            "                                                     data-trix-attribute=\"heading1\" title=\"Heading\" tabindex=\"-1\">Heading</button>\n" +
            "                                             <button type=\"button\" class=\"trix-button trix-button--icon trix-button--icon-quote\" data-trix-attribute=\"quote\"\n" +
            "                                                     title=\"Quote\" tabindex=\"-1\">Quote</button>\n" +
            "                                             <button type=\"button\" class=\"trix-button trix-button--icon trix-button--icon-code\" data-trix-attribute=\"code\"\n" +
            "                                                     title=\"Code\" tabindex=\"-1\">Code</button>\n" +
            "                                             <button type=\"button\" class=\"trix-button trix-button--icon trix-button--icon-bullet-list\"\n" +
            "                                                     data-trix-attribute=\"bullet\" title=\"Bullets\" tabindex=\"-1\">Bullets</button>\n" +
            "                                             <button type=\"button\" class=\"trix-button trix-button--icon trix-button--icon-number-list\"\n" +
            "                                                     data-trix-attribute=\"number\" title=\"Numbers\" tabindex=\"-1\">Numbers</button>\n" +
            "                                             <button type=\"button\" class=\"trix-button trix-button--icon trix-button--icon-decrease-nesting-level\"\n" +
            "                                                     data-trix-action=\"decreaseNestingLevel\" title=\"Decrease Level\" tabindex=\"-1\">Decrease Level</button>\n" +
            "                                             <button type=\"button\" class=\"trix-button trix-button--icon trix-button--icon-increase-nesting-level\"\n" +
            "                                                     data-trix-action=\"increaseNestingLevel\" title=\"Increase Level\" tabindex=\"-1\">Increase Level</button>\n" +
            "                                             </span>\n" +
            "                                                    <span class=\"trix-button-group trix-button-group--file-tools\"\n" +
            "                                                          data-trix-button-group=\"file-tools\">\n" +
            "                                             <button type=\"button\" class=\"trix-button trix-button--icon trix-button--icon-attach\" data-trix-action=\"attachFiles\"\n" +
            "                                                     title=\"Attach Files\" tabindex=\"-1\">Attach Files</button>\n" +
            "                                             </span>\n" +
            "                                                    <span class=\"trix-button-group-spacer\"></span>\n" +
            "                                                    <span class=\"trix-button-group trix-button-group--history-tools\"\n" +
            "                                                          data-trix-button-group=\"history-tools\">\n" +
            "                                             <button type=\"button\" class=\"trix-button trix-button--icon trix-button--icon-undo\" data-trix-action=\"undo\"\n" +
            "                                                     data-trix-key=\"z\" title=\"Undo\" tabindex=\"-1\">Undo</button>\n" +
            "                                             <button type=\"button\" class=\"trix-button trix-button--icon trix-button--icon-redo\" data-trix-action=\"redo\"\n" +
            "                                                     data-trix-key=\"shift+z\" title=\"Redo\" tabindex=\"-1\">Redo</button>\n" +
            "                                             </span>\n" +
            "                                                </div>\n" +
            "                                                <div class=\"trix-dialogs\" data-trix-dialogs=\"\">\n" +
            "                                                    <div class=\"trix-dialog trix-dialog--link\" data-trix-dialog=\"href\"\n" +
            "                                                         data-trix-dialog-attribute=\"href\">\n" +
            "                                                        <div class=\"trix-dialog__link-fields\">\n" +
            "                                                            <input type=\"url\" name=\"href\"\n" +
            "                                                                   class=\"trix-input trix-input--dialog\"\n" +
            "                                                                   placeholder=\"Enter a URL…\" aria-label=\"URL\"\n" +
            "                                                                   required=\"\" data-trix-input=\"\" disabled=\"disabled\">\n" +
            "                                                            <div class=\"trix-button-group\">\n" +
            "                                                                <input type=\"button\"\n" +
            "                                                                       class=\"trix-button trix-button--dialog\"\n" +
            "                                                                       value=\"Link\" data-trix-method=\"setAttribute\">\n" +
            "                                                                <input type=\"button\"\n" +
            "                                                                       class=\"trix-button trix-button--dialog\"\n" +
            "                                                                       value=\"Unlink\"\n" +
            "                                                                       data-trix-method=\"removeAttribute\">\n" +
            "                                                            </div>\n" +
            "                                                        </div>\n" +
            "                                                    </div>\n" +
            "                                                </div>\n" +
            "                                            </trix-toolbar>\n" +
            "                                            <trix-editor class=\"trix-content\" id='description_trix-" + row +"' \n" +
            "                                                         input='6011882f-12ce-4f9b-8b45-3cb410c4ff67-" + row + "'\n" +
            "                                                         data-test=\"description-field\"\n" +
            "                                                         placeholder=\"Describe what you'll be doing…\" contenteditable=\"\"\n" +
            "                                                         role=\"textbox\" trix-id=\"10\"\n" +
            "                                                         toolbar='trix-toolbar-10" + row + "'></trix-editor>\n" +
            "                                        </div>\n" +
            "                                    </div>\n" +
            "                                </div>\n" +
            "                                <div class=\"pwrk-frm__item-options\">\n" +
            // "                                    <span class=\"u-visible-lrg-scrn-only\">\n" +
            // "                                    <a\n" +
            // "                                            class=\"btn btn--sml btn--icon-only u-mrgn-r--x2\"\n" +
            // "                                            data-tooltip=\"Drag to reorder\">\n" +
            // "                                       <span class=\"icon icon--tiny\">\n" +
            // "                                          <svg\n" +
            // "                                                  xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\">\n" +
            // "                                             <path\n" +
            // "                                                     d=\"M12 23.998a.755.755 0 0 1-.26-.047l-.022-.008a.732.732 0 0 1-.249-.165l-3.749-3.75a.744.744 0 0 1 0-1.06.744.744 0 0 1 1.06 0l2.47 2.47V2.559l-2.47 2.47a.744.744 0 0 1-1.06-.001c-.142-.141-.22-.33-.22-.53s.078-.389.22-.53L11.469.219a.74.74 0 0 1 .245-.163l.025-.009a.733.733 0 0 1 .521.001l.021.007c.097.04.179.095.25.166L16.28 3.97c.142.141.22.33.22.53s-.078.389-.22.53a.749.749 0 0 1-1.06 0l-2.47-2.47v18.879l2.47-2.47a.744.744 0 0 1 1.06 0 .749.749 0 0 1 0 1.06l-3.75 3.75a.763.763 0 0 1-.246.164l-.027.01a.769.769 0 0 1-.257.045z\"></path>\n" +
            // "                                          </svg>\n" +
            // "                                       </span>\n" +
            // "                                    </a>\n" +
            // "                                 </span>\n" +
            "                                    <button data-test=\"remove-item-button\" class=\"btn remove-item btn--sml btn--icon-only\"\n" +
            "                                            type=\"button\" data-tooltip=\"Delete this item\">\n" +
            "                                    <span class=\"icon icon--tiny\">\n" +
            "                                       <svg\n" +
            "                                               xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\">\n" +
            "                                          <path\n" +
            "                                                  d=\"M6.631 23.25a2.263 2.263 0 0 1-2.242-2.064L3.06 5.25H1.5a.75.75 0 0 1 0-1.5h6V3A2.252 2.252 0 0 1 9.75.75h4.5A2.252 2.252 0 0 1 16.5 3v.75h6a.75.75 0 0 1 0 1.5h-1.56l-1.328 15.937a2.262 2.262 0 0 1-2.242 2.063H6.631zm-.748-2.188c.032.386.36.688.748.688H17.37a.753.753 0 0 0 .747-.688L19.435 5.25H4.565l1.318 15.812zM15 3.75V3a.75.75 0 0 0-.75-.75h-4.5A.75.75 0 0 0 9 3v.75h6z\"></path>\n" +
            "                                          <path\n" +
            "                                                  d=\"M9.75 18a.75.75 0 0 1-.75-.75v-7.5a.75.75 0 0 1 1.5 0v7.5a.75.75 0 0 1-.75.75zm4.5 0a.75.75 0 0 1-.75-.75v-7.5a.75.75 0 0 1 1.5 0v7.5a.75.75 0 0 1-.75.75z\"></path>\n" +
            "                                       </svg>\n" +
            "                                    </span>\n" +
            "                                    </button>\n" +
            "                                </div>\n" +
            "                            </div>\n" +
            "                            <div class=\"pwrk-frm__item-amounts\">\n" +
            "                                <div class=\"pwrk-frm__item-prices\">\n" +
            "                                    <a role=\"button\" class=\"link link--modal\"\n" +
            "                                       tabindex=\"0\" data-test=\"materials-button\">\n" +
            "                                        <div class=\"pwrk-frm__item-price\" data-test=\"item-materials-total\">\n" +
            "                                            <label>Materials</label><span class=\"pwrk-frm__item-price-input material_popup\"><span\n" +
            "                                                class=\"input--currency\" data-currency-unit=\"£\"><input disabled=\"\"\n" +
            "                                                                                                      class=\"pwrk-frm__input material_input pwrk-frm__input--pseudo\"\n" +
            "                                                                                                      value=\"0.00\"></span></span>\n" +
            "                                        </div>\n" +
            "                                    </a>\n" +
            "                                    <a role=\"button\" class=\"link link--modal\" tabindex=\"0\" data-test=\"labour-button\">\n" +
            "                                        <div class=\"pwrk-frm__item-price\" data-test=\"item-labour-total\">\n" +
            "                                            <label>Labour</label><span class=\"pwrk-frm__item-price-input labour_popup\"><span\n" +
            "                                                class=\"input--currency\" data-currency-unit=\"£\"><input disabled=\"\"\n" +
            "                                                                                                      class=\"pwrk-frm__input pwrk-frm__input--pseudo\"\n" +
            "                                                                                                      value=\"0.00\"></span></span>\n" +
            "                                        </div>\n" +
            "                                    </a>\n" +
            "                                    <div class=\"pwrk-frm__item-price\"><label for=\"price\">Price</label><span\n" +
            "                                            class=\"pwrk-frm__item-price-input\"><span class=\"input--currency\"\n" +
            "                                                                                     data-currency-unit=\"£\"><input\n" +
            "                                            name=\"price\" type=\"number\" placeholder=\"0.00\" autocomplete=\"off\"\n" +
            "                                            inputmode=\"decimal\" maxlength=\"12\" class=\"pwrk-frm__input \"\n" +
            "                                            data-test=\"price-field\" id=\"price\" value=\"0.00\"></span></span></div>\n" +
            "                                </div>\n" +
            "                                <div class=\"pwrk-frm__item-totals\">\n" +
            "                                    <div class=\"pwrk-frm__item-total\"><label>Sub Total</label><span\n" +
            "                                            class=\"pwrk-frm__item-total-value subtotal\" data-test=\"item-sub-total\">£0.00</span>\n" +
            "                                    </div>\n" +
            "                                    <div class=\"pwrk-frm__item-total\">\n" +
            "                                        <label for=\"taxRate\">VAT</label>\n" +
            "                                        <span\n" +
            "                                                class=\"pwrk-frm__item-total-select\">\n" +
            "                                       <div>\n" +
            "                                          <select name=\"taxRate\" id=\"taxRate\"\n" +
            "                                                  data-test=\"tax-rate-field\">\n" +
            "                                             <option\n" +
            "                                                     value=\"0.00\">0%</option>\n" +
            "                                             <option\n" +
            "                                                     value=\"5\">5%</option>\n" +
            "                                             <option\n" +
            "                                                     value=\"20\">20%</option>\n" +
            "                                          </select>\n" +
            "                                       </div>\n" +
            "                                    </span>\n" +
            "                                    </div>\n" +
            "                                    <div class=\"pwrk-frm__item-total pwrk-frm__item-total--bold\">\n" +
            "                                        <label>Total</label><span\n" +
            "                                            class=\"pwrk-frm__item-total-value total pwrk-frm__item-total--bold\"\n" +
            "                                            data-test=\"item-total\">£0.00</span>\n" +
            "                                    </div>\n" +
            "                                </div>\n" +
            "                            </div>\n" +
            "                        </div>";
    }

    getMaterialTemplate(object_id, row){
        let material_row_template = '';

        if(this.materials.length > 0){
            window.localStorage.setItem('exists_material', 1);
            this.materials.forEach(function(item, index){
                material_row_template += '<ul data-row="'+index+'" class="m-table__row m-table__row--empty" data-test="material-item">\n' +
                    '   <li data-label="Material" class="m-table__cell m-table__cell--title">\n' +
                    // '      <a class="m-table__row-drag-handle">\n' +
                    // '         <span class="icon icon--tiny">\n' +
                    // '            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">\n' +
                    // '               <path d="M12 23.998a.755.755 0 0 1-.26-.047l-.022-.008a.732.732 0 0 1-.249-.165l-3.749-3.75a.744.744 0 0 1 0-1.06.744.744 0 0 1 1.06 0l2.47 2.47V2.559l-2.47 2.47a.744.744 0 0 1-1.06-.001c-.142-.141-.22-.33-.22-.53s.078-.389.22-.53L11.469.219a.74.74 0 0 1 .245-.163l.025-.009a.733.733 0 0 1 .521.001l.021.007c.097.04.179.095.25.166L16.28 3.97c.142.141.22.33.22.53s-.078.389-.22.53a.749.749 0 0 1-1.06 0l-2.47-2.47v18.879l2.47-2.47a.744.744 0 0 1 1.06 0 .749.749 0 0 1 0 1.06l-3.75 3.75a.763.763 0 0 1-.246.164l-.027.01a.769.769 0 0 1-.257.045z"></path>\n' +
                    // '            </svg>\n' +
                    // '         </span>\n' +
                    // '      </a>\n' +
                    '      <input name="title" placeholder="Enter material description" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m-table__input material_title m-table__input--title" data-test="description-field" id="description" value="'+ item.title +'">\n' +
                    '      <div></div>\n' +
                    '   </li>\n' +
                    '   <li data-label="Qty" class="m-table__cell m-table__cell--qty"><input name="quantity" placeholder="0" autocomplete="off" inputmode="" maxlength="" class="material_qty pwrk-frm__input m-table__input m-table__input--qty" data-test="quantity-field" id="quantity" value="'+ item.qty +'"></li>\n' +
                    '   <li data-label="Unit type" class="m-table__cell m-table__cell--units"><input name="unitType" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m_type m-table__input" data-test="unit-type-field" id="unitType" value="'+ item.type +'"></li>\n' +
                    '   <li data-label="£ per unit" class="m-table__cell m-table__cell--price"><span class="input--currency" data-currency-unit="£"><input name="itemPrice" type="number" placeholder="0.00" autocomplete="off" inputmode="decimal" maxlength="12" class="pwrk-frm__input m-table__input m-table__input--price price-input" data-test="item-price-field" id="itemPrice" value="'+ item.price +'"></span></li>\n' +
                    '   <li data-label="% markup" class="m-table__cell m-table__cell--markup"><span class="input--percentage"><input name="markupPercentage" placeholder="0" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m-table__input m-table__input--markup percent-input" data-test="markup-percentage-field" id="markupPercentage" value="'+ item.markup +'"></span></li>\n' +
                    '   <li data-label="Total" class="m-table__cell m-table__cell--total"><span class="m-table__input--total" data-test="material-item-total">£'+ item.total +'</span></li>\n' +
                    '   <li class="m-table__cell m-table__cell--actions">\n' +
                    '      <button data-test="remove-material-item-button" class="btn btn--tiny btn--icon-only" type="button">\n' +
                    '         <span class="icon icon--tiny">\n' +
                    '            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">\n' +
                    '               <path d="M6.631 23.25a2.263 2.263 0 0 1-2.242-2.064L3.06 5.25H1.5a.75.75 0 0 1 0-1.5h6V3A2.252 2.252 0 0 1 9.75.75h4.5A2.252 2.252 0 0 1 16.5 3v.75h6a.75.75 0 0 1 0 1.5h-1.56l-1.328 15.937a2.262 2.262 0 0 1-2.242 2.063H6.631zm-.748-2.188c.032.386.36.688.748.688H17.37a.753.753 0 0 0 .747-.688L19.435 5.25H4.565l1.318 15.812zM15 3.75V3a.75.75 0 0 0-.75-.75h-4.5A.75.75 0 0 0 9 3v.75h6z"></path>\n' +
                    '               <path d="M9.75 18a.75.75 0 0 1-.75-.75v-7.5a.75.75 0 0 1 1.5 0v7.5a.75.75 0 0 1-.75.75zm4.5 0a.75.75 0 0 1-.75-.75v-7.5a.75.75 0 0 1 1.5 0v7.5a.75.75 0 0 1-.75.75z"></path>\n' +
                    '            </svg>\n' +
                    '         </span>\n' +
                    '      </button>\n' +
                    '   </li>\n' +
                    '</ul>';
            });
        }

        if (this.materials.filter((item) => item != null).length < 1) {
            material_row_template +=  '                        <ul data-row="' + row + '" class="m-table__row m-table__row--empty" data-test="material-item">\n' +
                '                            <li data-label="Material" class="m-table__cell m-table__cell--title">\n' +
                '                                <a class="m-table__row-drag-handle">\n' +
                '                        <span class="icon icon--tiny">\n' +
                '                           <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">\n' +
                '                              <path d="M12 23.998a.755.755 0 0 1-.26-.047l-.022-.008a.732.732 0 0 1-.249-.165l-3.749-3.75a.744.744 0 0 1 0-1.06.744.744 0 0 1 1.06 0l2.47 2.47V2.559l-2.47 2.47a.744.744 0 0 1-1.06-.001c-.142-.141-.22-.33-.22-.53s.078-.389.22-.53L11.469.219a.74.74 0 0 1 .245-.163l.025-.009a.733.733 0 0 1 .521.001l.021.007c.097.04.179.095.25.166L16.28 3.97c.142.141.22.33.22.53s-.078.389-.22.53a.749.749 0 0 1-1.06 0l-2.47-2.47v18.879l2.47-2.47a.744.744 0 0 1 1.06 0 .749.749 0 0 1 0 1.06l-3.75 3.75a.763.763 0 0 1-.246.164l-.027.01a.769.769 0 0 1-.257.045z"></path>\n' +
                '                           </svg>\n' +
                '                        </span>\n' +
                '                                </a>\n' +
                '                                <input name="title" placeholder="Enter material description" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m-table__input material_title m-table__input--title" data-test="description-field" id="description" value="">\n' +
                '                                <div></div>\n' +
                '                            </li>\n' +
                '                            <li data-label="Qty" class="m-table__cell m-table__cell--qty"><input name="quantity" placeholder="0" autocomplete="off" inputmode="" maxlength="" class="material_qty pwrk-frm__input m-table__input m-table__input--qty" data-test="quantity-field" id="quantity" value="1"></li>\n' +
                '                            <li data-label="Unit type" class="m-table__cell m-table__cell--units"><input name="unitType" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m_type m-table__input" data-test="unit-type-field" id="unitType" value=""></li>\n' +
                '                            <li data-label="£ per unit" class="m-table__cell m-table__cell--price"><span class="input--currency" data-currency-unit="£"><input name="itemPrice" type="number" placeholder="0.00" autocomplete="off" inputmode="decimal" maxlength="12" class="pwrk-frm__input m-table__input m-table__input--price price-input" data-test="item-price-field" id="itemPrice" value="0.00"></span></li>\n' +
                '                            <li data-label="% markup" class="m-table__cell m-table__cell--markup"><span class="input--percentage"><input name="markupPercentage" placeholder="0" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m-table__input m-table__input--markup percent-input" data-test="markup-percentage-field" id="markupPercentage" value="0.0"></span></li>\n' +
                '                            <li data-label="Total" class="m-table__cell m-table__cell--total"><span class="m-table__input--total" data-test="material-item-total">£0.00</span></li>\n' +
                '                            <li class="m-table__cell m-table__cell--actions">\n' +
                '                                <button data-test="remove-material-item-button" class="btn btn--tiny btn--icon-only" type="button">\n' +
                '                        <span class="icon icon--tiny">\n' +
                '                           <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">\n' +
                '                              <path d="M6.631 23.25a2.263 2.263 0 0 1-2.242-2.064L3.06 5.25H1.5a.75.75 0 0 1 0-1.5h6V3A2.252 2.252 0 0 1 9.75.75h4.5A2.252 2.252 0 0 1 16.5 3v.75h6a.75.75 0 0 1 0 1.5h-1.56l-1.328 15.937a2.262 2.262 0 0 1-2.242 2.063H6.631zm-.748-2.188c.032.386.36.688.748.688H17.37a.753.753 0 0 0 .747-.688L19.435 5.25H4.565l1.318 15.812zM15 3.75V3a.75.75 0 0 0-.75-.75h-4.5A.75.75 0 0 0 9 3v.75h6z"></path>\n' +
                '                              <path d="M9.75 18a.75.75 0 0 1-.75-.75v-7.5a.75.75 0 0 1 1.5 0v7.5a.75.75 0 0 1-.75.75zm4.5 0a.75.75 0 0 1-.75-.75v-7.5a.75.75 0 0 1 1.5 0v7.5a.75.75 0 0 1-.75.75z"></path>\n' +
                '                           </svg>\n' +
                '                        </span>\n' +
                '                                </button>\n' +
                '                            </li>\n' +
                '                        </ul>\n';
        }

        return '<div class="material_popup_wrapper"  data-object_id="'+ object_id +'">\n' +
            '        <div class="c-panel" data-object_id="'+ object_id +'">\n' +
            '            <button class="c-panel__close-btn close_material_popup btn btn--sml btn--white" type="button" data-test="close-button">Done</button>\n' +
            '            <div class="c-panel__inner">\n' +
            '                <div class="c-panel__header c-panel__header--fixed">\n' +
            '                    <h1 class="t--lrg u-mrgn-btm" data-test="material-panel-title"><strong>Material costs</strong></h1>\n' +
            '                    <h2 class="t--reg"><span data-test="material-items-total">£0.00</span>&nbsp; · &nbsp;<span class="c--dark-metal" data-test="material-items-subtotal">£0.00 before markup</span></h2>\n' +
            '                </div>\n' +
            '                <div class="m-table " data-test="material-items">\n' +
            '                    <div>\n' + material_row_template +
            '                    </div>\n' +
            '                </div>\n' +
            '                <div class="pwrk-frm__add-section-holder">\n' +
            '                    <button data-test="add-material-button" class="pwrk-frm__add-section add_material_button pwrk-frm__add-section--panel-row" type="button">\n' +
            '               <span class="icon icon--green u-mrgn-r--x2">\n' +
            '                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">\n' +
            '                     <title>add-circle</title>\n' +
            '                     <path d="M12 17.25a.75.75 0 0 1-.75-.75v-3.75H7.5a.75.75 0 0 1 0-1.5h3.75V7.5a.75.75 0 0 1 1.5 0v3.75h3.75a.75.75 0 0 1 0 1.5h-3.75v3.75a.75.75 0 0 1-.75.75z"></path>\n' +
            '                     <path d="M12 24C5.383 24 0 18.617 0 12S5.383 0 12 0s12 5.383 12 12-5.383 12-12 12zm0-22.5C6.21 1.5 1.5 6.21 1.5 12S6.21 22.5 12 22.5 22.5 17.79 22.5 12 17.79 1.5 12 1.5z"></path>\n' +
            '                  </svg>\n' +
            '               </span>\n' +
            '                        Add material row\n' +
            '                    </button>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '        <div class="c-panel__overlay">&nbsp;</div>\n' +
            '    </div>'
    }

    getLabourTemplate(object_id, row) {
        let material_row_template = '';

        if(this.labours.length > 0){
            window.localStorage.setItem('exists_material', 1);
            this.labours.forEach(function(item, index){
                console.log(item.qty)
                const select = `
                <select name="unitType" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m-table__input m_type" data-test="unit-type-field" id="unitType">
                    <option ${item.type === 'hours' ? 'selected' : ''} value="test">Hours</option>
                    <option ${item.type === 'days' ? 'selected' : ''} value="test2">Days</option>
                    <option ${item.type === 'sq.mtr.' ? 'selected' : ''} value="test2">Sq. Mtr.</option>
                    <option ${item.type === 'meter' ? 'selected' : ''} value="test2">Meter</option>
                    <option ${item.type === 'sq. ft.' ? 'selected' : ''} value="test2">Sq. Ft.</option>
                    <option ${item.type === 'ft' ? 'selected' : ''} value="test2">Ft</option>
                    <option ${item.type === 'units' ? 'selected' : ''} value="test2">Units</option>
                    <option ${item.type === 'each' ? 'selected' : ''} value="test2">Each</option>
                    <option ${item.type === 'minutes' ? 'selected' : ''} value="test2">Minutes</option>
                 </select
                `;
                material_row_template += '<ul data-row="'+index+'" class="m-table__row m-table__row--empty" data-test="material-item">\n' +
                    '   <li data-label="Material" class="m-table__cell m-table__cell--title">\n' +
                    // '      <a class="m-table__row-drag-handle">\n' +
                    // '         <span class="icon icon--tiny">\n' +
                    // '            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">\n' +
                    // '               <path d="M12 23.998a.755.755 0 0 1-.26-.047l-.022-.008a.732.732 0 0 1-.249-.165l-3.749-3.75a.744.744 0 0 1 0-1.06.744.744 0 0 1 1.06 0l2.47 2.47V2.559l-2.47 2.47a.744.744 0 0 1-1.06-.001c-.142-.141-.22-.33-.22-.53s.078-.389.22-.53L11.469.219a.74.74 0 0 1 .245-.163l.025-.009a.733.733 0 0 1 .521.001l.021.007c.097.04.179.095.25.166L16.28 3.97c.142.141.22.33.22.53s-.078.389-.22.53a.749.749 0 0 1-1.06 0l-2.47-2.47v18.879l2.47-2.47a.744.744 0 0 1 1.06 0 .749.749 0 0 1 0 1.06l-3.75 3.75a.763.763 0 0 1-.246.164l-.027.01a.769.769 0 0 1-.257.045z"></path>\n' +
                    // '            </svg>\n' +
                    // '         </span>\n' +
                    // '      </a>\n' +
                    '      <input name="title" placeholder="Enter material description" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m-table__input material_title m-table__input--title" data-test="description-field" id="description" value="'+ item.title +'">\n' +
                    '      <div></div>\n' +
                    '   </li>\n' +
                    '   <li data-label="Qty" class="m-table__cell m-table__cell--qty"><input name="quantity" placeholder="0" autocomplete="off" inputmode="" maxlength="" class="material_qty pwrk-frm__input m-table__input m-table__input--qty" data-test="quantity-field" id="quantity" value="'+ item.qty +'"></li>\n' +
                    '   <li data-label="Unit type" class="m-table__cell m-table__cell--units">' + select +
                    '</li>\n' +
                    '   <li data-label="£ per unit" class="m-table__cell m-table__cell--price"><span class="input--currency" data-currency-unit="£"><input name="itemPrice" type="number" placeholder="0.00" autocomplete="off" inputmode="decimal" maxlength="12" class="pwrk-frm__input m-table__input m-table__input--price price-input" data-test="item-price-field" id="itemPrice" value="'+ item.price +'"></span></li>\n' +
                    '   <li data-label="% markup" class="m-table__cell m-table__cell--markup"><span class="input--percentage"><input name="markupPercentage" placeholder="0" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m-table__input m-table__input--markup percent-input" data-test="markup-percentage-field" id="markupPercentage" value="'+ item.markup +'"></span></li>\n' +
                    '   <li data-label="Total" class="m-table__cell m-table__cell--total"><span class="m-table__input--total" data-test="material-item-total">£'+ item.total +'</span></li>\n' +
                    '   <li class="m-table__cell m-table__cell--actions">\n' +
                    '      <button data-test="remove-material-item-button" class="btn btn--tiny btn--icon-only" type="button">\n' +
                    '         <span class="icon icon--tiny">\n' +
                    '            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">\n' +
                    '               <path d="M6.631 23.25a2.263 2.263 0 0 1-2.242-2.064L3.06 5.25H1.5a.75.75 0 0 1 0-1.5h6V3A2.252 2.252 0 0 1 9.75.75h4.5A2.252 2.252 0 0 1 16.5 3v.75h6a.75.75 0 0 1 0 1.5h-1.56l-1.328 15.937a2.262 2.262 0 0 1-2.242 2.063H6.631zm-.748-2.188c.032.386.36.688.748.688H17.37a.753.753 0 0 0 .747-.688L19.435 5.25H4.565l1.318 15.812zM15 3.75V3a.75.75 0 0 0-.75-.75h-4.5A.75.75 0 0 0 9 3v.75h6z"></path>\n' +
                    '               <path d="M9.75 18a.75.75 0 0 1-.75-.75v-7.5a.75.75 0 0 1 1.5 0v7.5a.75.75 0 0 1-.75.75zm4.5 0a.75.75 0 0 1-.75-.75v-7.5a.75.75 0 0 1 1.5 0v7.5a.75.75 0 0 1-.75.75z"></path>\n' +
                    '            </svg>\n' +
                    '         </span>\n' +
                    '      </button>\n' +
                    '   </li>\n' +
                    '</ul>';
            });
        }
        const select = `
                <select name="unitType" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m-table__input m_type" data-test="unit-type-field" id="unitType">
                    <option value="test">Hours</option>
                    <option value="test2">Days</option>
                    <option value="test2">Sq. Mtr.</option>
                    <option value="test2">Meter</option>
                    <option value="test2">Sq. Ft.</option>
                    <option value="test2">Ft</option>
                    <option value="test2">Units</option>
                    <option value="test2">Each</option>
                    <option value="test2">Minutes</option>
                 </select
                `;

        if (this.labours.filter((item) => item != null).length < 1) {
            material_row_template += '                        <ul data-row="' + row + '" class="m-table__row m-table__row--empty" data-test="material-item">\n' +
                '                            <li data-label="Material" class="m-table__cell m-table__cell--title">\n' +
                '                                <a class="m-table__row-drag-handle">\n' +
                '                        <span class="icon icon--tiny">\n' +
                '                           <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">\n' +
                '                              <path d="M12 23.998a.755.755 0 0 1-.26-.047l-.022-.008a.732.732 0 0 1-.249-.165l-3.749-3.75a.744.744 0 0 1 0-1.06.744.744 0 0 1 1.06 0l2.47 2.47V2.559l-2.47 2.47a.744.744 0 0 1-1.06-.001c-.142-.141-.22-.33-.22-.53s.078-.389.22-.53L11.469.219a.74.74 0 0 1 .245-.163l.025-.009a.733.733 0 0 1 .521.001l.021.007c.097.04.179.095.25.166L16.28 3.97c.142.141.22.33.22.53s-.078.389-.22.53a.749.749 0 0 1-1.06 0l-2.47-2.47v18.879l2.47-2.47a.744.744 0 0 1 1.06 0 .749.749 0 0 1 0 1.06l-3.75 3.75a.763.763 0 0 1-.246.164l-.027.01a.769.769 0 0 1-.257.045z"></path>\n' +
                '                           </svg>\n' +
                '                        </span>\n' +
                '                                </a>\n' +
                '                                <input name="title" placeholder="Enter material description" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m-table__input material_title m-table__input--title" data-test="description-field" id="description" value="">\n' +
                '                                <div></div>\n' +
                '                            </li>\n' +
                '                            <li data-label="Qty" class="m-table__cell m-table__cell--qty"><input name="quantity" placeholder="0" autocomplete="off" inputmode="" maxlength="" class="material_qty pwrk-frm__input m-table__input m-table__input--qty" data-test="quantity-field" id="quantity" value="1"></li>\n' +
                '                            <li data-label="Unit type" class="m-table__cell m-table__cell--units">' + select +          '</li>\n' +
                '                            <li data-label="£ per unit" class="m-table__cell m-table__cell--price"><span class="input--currency" data-currency-unit="£"><input name="itemPrice" type="number" placeholder="0.00" autocomplete="off" inputmode="decimal" maxlength="12" class="pwrk-frm__input m-table__input m-table__input--price price-input" data-test="item-price-field" id="itemPrice" value="0.00"></span></li>\n' +
                '                            <li data-label="% markup" class="m-table__cell m-table__cell--markup"><span class="input--percentage"><input name="markupPercentage" placeholder="0" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m-table__input m-table__input--markup percent-input" data-test="markup-percentage-field" id="markupPercentage" value="0.0"></span></li>\n' +
                '                            <li data-label="Total" class="m-table__cell m-table__cell--total"><span class="m-table__input--total" data-test="material-item-total">£0.00</span></li>\n' +
                '                            <li class="m-table__cell m-table__cell--actions">\n' +
                '                                <button data-test="remove-material-item-button" class="btn btn--tiny btn--icon-only" type="button">\n' +
                '                        <span class="icon icon--tiny">\n' +
                '                           <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">\n' +
                '                              <path d="M6.631 23.25a2.263 2.263 0 0 1-2.242-2.064L3.06 5.25H1.5a.75.75 0 0 1 0-1.5h6V3A2.252 2.252 0 0 1 9.75.75h4.5A2.252 2.252 0 0 1 16.5 3v.75h6a.75.75 0 0 1 0 1.5h-1.56l-1.328 15.937a2.262 2.262 0 0 1-2.242 2.063H6.631zm-.748-2.188c.032.386.36.688.748.688H17.37a.753.753 0 0 0 .747-.688L19.435 5.25H4.565l1.318 15.812zM15 3.75V3a.75.75 0 0 0-.75-.75h-4.5A.75.75 0 0 0 9 3v.75h6z"></path>\n' +
                '                              <path d="M9.75 18a.75.75 0 0 1-.75-.75v-7.5a.75.75 0 0 1 1.5 0v7.5a.75.75 0 0 1-.75.75zm4.5 0a.75.75 0 0 1-.75-.75v-7.5a.75.75 0 0 1 1.5 0v7.5a.75.75 0 0 1-.75.75z"></path>\n' +
                '                           </svg>\n' +
                '                        </span>\n' +
                '                                </button>\n' +
                '                            </li>\n' +
                '                        </ul>\n';
        }
            return '  <div class="material_popup_wrapper"  data-object_id="'+ object_id +'">\n' +
            '        <div class="c-panel" data-object_id="'+ object_id +'">\n' +
            '            <button class="c-panel__close-btn close_material_popup btn btn--sml btn--white" type="button" data-test="close-button">Done</button>\n' +
            '            <div class="c-panel__inner">\n' +
            '                <div class="c-panel__header c-panel__header--fixed">\n' +
            '                    <h1 class="t--lrg u-mrgn-btm" data-test="material-panel-title"><strong>Material costs</strong></h1>\n' +
            '                    <h2 class="t--reg"><span data-test="material-items-total">£0.00</span>&nbsp; · &nbsp;<span class="c--dark-metal" data-test="material-items-subtotal">£0.00 before markup</span></h2>\n' +
            '                </div>\n' +
            '                <div class="m-table " data-test="material-items">\n' +
            '                    <div>\n' + material_row_template +
            '                    </div>\n' +
            '                </div>\n' +
            '                <div class="pwrk-frm__add-section-holder">\n' +
            '                    <button data-test="add-material-button" class="pwrk-frm__add-section add_material_button pwrk-frm__add-section--panel-row" type="button">\n' +
            '               <span class="icon icon--green u-mrgn-r--x2">\n' +
            '                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">\n' +
            '                     <title>add-circle</title>\n' +
            '                     <path d="M12 17.25a.75.75 0 0 1-.75-.75v-3.75H7.5a.75.75 0 0 1 0-1.5h3.75V7.5a.75.75 0 0 1 1.5 0v3.75h3.75a.75.75 0 0 1 0 1.5h-3.75v3.75a.75.75 0 0 1-.75.75z"></path>\n' +
            '                     <path d="M12 24C5.383 24 0 18.617 0 12S5.383 0 12 0s12 5.383 12 12-5.383 12-12 12zm0-22.5C6.21 1.5 1.5 6.21 1.5 12S6.21 22.5 12 22.5 22.5 17.79 22.5 12 17.79 1.5 12 1.5z"></path>\n' +
            '                  </svg>\n' +
            '               </span>\n' +
            '                        Add material row\n' +
            '                    </button>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '        <div class="c-panel__overlay">&nbsp;</div>\n' +
            '    </div>'
    }

    existMaterial(key, value){

        if(this.materials.length < 0){
            return false;
        }
        let bool = false;
        this.materials.forEach(function(item){
            if(item[key].localeCompare(value) === 0){
                bool = true;
            };
        });

        return bool;

    }

    findMaterial(key, value){
        if(this.materials.length === 0){
            return false;
        }
        let material ;
        this.materials.forEach(function(item){
            if(item[key].localeCompare(value) === 0){
                material = item;
            };
        });

        return material;
    }

    calculateSubTotal(){
        console.log(this.price);
        console.log(this.material_amount);
        console.log(this.labours_amount);
        this.subtotal = this.price + this.material_amount + this.labours_amount;
        console.log(this.subtotal);
        return this.subtotal;
    }

    calculateTotal(){
        if(this.tax_rate === 0){
            this.total = this.subtotal;
            return this.subtotal
        }

        this.total = this.subtotal * this.tax_rate / 100 + this.subtotal;

        return this.total;
    }

    getTitle(){
        return this.title;
    }

    setDescription(description){
        this.description = description
    }

    setPrice(price){
        this.price = price;
    }

    setTaxRate(tax_rate){
        this.tax_rate = tax_rate;
    }

    setTitle(title){
        this.title = title;
    }

    setMaterials(materials){
        // this.materials = this.materials.concat(materials);
        this.materials = materials;
    }

    setLabours(labours) {
        this.labours = labours
    }

    calculateMaterialAmount() {
        if (!this.materials) {
            return 0;
        }

        let sum=0;

        this.materials.forEach(function(item) {
           sum += item.total;
        });


        this.material_amount = sum;
    }

    calculateLaboursAmount() {
        if (!this.labours) {
            return 0;
        }

        let sum=0;

        this.labours.forEach(function(item) {
            sum += item.total;
        });


        this.labours_amount = sum;
    }
}

class MaterialItem{
    constructor(id, title, qty = 1, type, price = 0, markup = 0, total = 0){
        this.title = title;
        this.qty = qty;
        this.type = type;
        this.price = price;
        this.markup = markup;
        this.total = total;
        this.id = id;
    }

    setId(id){
        this.id = parseInt(id);
    }

    static getMaterialRow(row){
        return '<ul data-row="' + row + '" id="row-' + row +'" class="m-table__row m-table__row--empty" data-test="material-item">\n' +
            '   <li data-label="Material" class="m-table__cell m-table__cell--title">\n' +
            // '      <a class="m-table__row-drag-handle">\n' +
            // '         <span class="icon icon--tiny">\n' +
            // '            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">\n' +
            // '               <path d="M12 23.998a.755.755 0 0 1-.26-.047l-.022-.008a.732.732 0 0 1-.249-.165l-3.749-3.75a.744.744 0 0 1 0-1.06.744.744 0 0 1 1.06 0l2.47 2.47V2.559l-2.47 2.47a.744.744 0 0 1-1.06-.001c-.142-.141-.22-.33-.22-.53s.078-.389.22-.53L11.469.219a.74.74 0 0 1 .245-.163l.025-.009a.733.733 0 0 1 .521.001l.021.007c.097.04.179.095.25.166L16.28 3.97c.142.141.22.33.22.53s-.078.389-.22.53a.749.749 0 0 1-1.06 0l-2.47-2.47v18.879l2.47-2.47a.744.744 0 0 1 1.06 0 .749.749 0 0 1 0 1.06l-3.75 3.75a.763.763 0 0 1-.246.164l-.027.01a.769.769 0 0 1-.257.045z"></path>\n' +
            // '            </svg>\n' +
            // '         </span>\n' +
            // '      </a>\n' +
            '      <input name="title" placeholder="Enter material description" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m-table__input material_title m-table__input--title" data-test="description-field" id="description" value="">\n' +
            '      <div></div>\n' +
            '   </li>\n' +
            '   <li data-label="Qty" class="m-table__cell m-table__cell--qty"><input name="quantity" placeholder="0" autocomplete="off" inputmode="" maxlength="" class="material_qty pwrk-frm__input m-table__input m-table__input--qty" data-test="quantity-field" id="quantity" value="1"></li>\n' +
            '   <li data-label="Unit type" class="m-table__cell m-table__cell--units"><input name="unitType" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m_type m-table__input" data-test="unit-type-field" id="unitType" value=""></li>\n' +
            '   <li data-label="£ per unit" class="m-table__cell m-table__cell--price"><span class="input--currency" data-currency-unit="£"><input name="itemPrice" type="number" placeholder="0.00" autocomplete="off" inputmode="decimal" maxlength="12" class="pwrk-frm__input m-table__input m-table__input--price price-input" data-test="item-price-field" id="itemPrice" value="0.00"></span></li>\n' +
            '   <li data-label="% markup" class="m-table__cell m-table__cell--markup"><span class="input--percentage"><input name="markupPercentage" placeholder="0" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m-table__input m-table__input--markup percent-input" data-test="markup-percentage-field" id="markupPercentage" value="0.0"></span></li>\n' +
            '   <li data-label="Total" class="m-table__cell m-table__cell--total"><span class="m-table__input--total" data-test="material-item-total">£0.00</span></li>\n' +
            '   <li class="m-table__cell m-table__cell--actions">\n' +
            '      <button data-test="remove-material-item-button" class="btn btn--tiny btn--icon-only" type="button">\n' +
            '         <span class="icon icon--tiny">\n' +
            '            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">\n' +
            '               <path d="M6.631 23.25a2.263 2.263 0 0 1-2.242-2.064L3.06 5.25H1.5a.75.75 0 0 1 0-1.5h6V3A2.252 2.252 0 0 1 9.75.75h4.5A2.252 2.252 0 0 1 16.5 3v.75h6a.75.75 0 0 1 0 1.5h-1.56l-1.328 15.937a2.262 2.262 0 0 1-2.242 2.063H6.631zm-.748-2.188c.032.386.36.688.748.688H17.37a.753.753 0 0 0 .747-.688L19.435 5.25H4.565l1.318 15.812zM15 3.75V3a.75.75 0 0 0-.75-.75h-4.5A.75.75 0 0 0 9 3v.75h6z"></path>\n' +
            '               <path d="M9.75 18a.75.75 0 0 1-.75-.75v-7.5a.75.75 0 0 1 1.5 0v7.5a.75.75 0 0 1-.75.75zm4.5 0a.75.75 0 0 1-.75-.75v-7.5a.75.75 0 0 1 1.5 0v7.5a.75.75 0 0 1-.75.75z"></path>\n' +
            '            </svg>\n' +
            '         </span>\n' +
            '      </button>\n' +
            '   </li>\n' +
            '</ul>'
    }

    setTitle(title){
        this.title = title;
    }

    setQty(qty){
        if (isNaN(qty)) {
            qty = 1;
        }

        this.qty = parseInt(qty);
    }

    setType(type){
        this.type = type;
    }

    setPrice(price){
        if (isNaN(price)) {
            price = 0;
        }

        this.price = parseFloat(price);
    }

    setMarkup(markup){
        this.markup = markup
    }

    calculateTotal(){
        this.subtotal = this.price * this.qty;

        if (this.subtotal < 0 || isNaN(this.subtotal)) {
            this.subtotal = 0;
        }

        if(this.markup === 0){
            this.total = this.subtotal
            return
        }

        this.total = this.subtotal * this.markup / 100 + this.subtotal;

        if (this.total < 0 || isNaN(this.total)) {
            this.total = 0;
        }
    }
}
//parent section were are situated all children
let parent_sections = $('.parent_section div')[0];
//create object - first children and append it
let first_section_item = new EstimateItem();
$(parent_sections).append(first_section_item.getTemplate(0));

let item_collection = [];
item_collection.push(first_section_item);
let i = 0;

const first_section = $('.pwrk-frm__section.pwrk-frm__item').first();
initEstimateSectionEvents(first_section);

$('.pwrk-frm__add-section').click(function () {
    i++;
    let new_item = new EstimateItem();
    let template = new_item.getTemplate(i);
    $(parent_sections).append(template);
    item_collection.push(new_item);
    const section = $('.pwrk-frm__section.pwrk-frm__item').last();

    initEstimateSectionEvents(section)
});

//add event and calculate material costs per item
function addEvent (id, material_collection, material_item_obj, event = 'click', quote_object = null) {
    $(id)[0].addEventListener(event, function() {

        $(this).find('.material_title').change(function () {
            let row = $(this).closest('ul').data('row');
            material_collection.splice(row, row, material_item_obj);
            material_collection[row].setTitle($(this).val());
        });

        $(this).find('.material_qty').on('change keydown paste input', function(){
            let row = $(this).closest('ul').data('row');
            material_collection.splice(row, row, material_item_obj);
            material_collection[row].setQty($(this).val());
            material_collection[row].calculateTotal();
            calculatePopupItemTotal($(this).closest('ul'));
            calculatePopupTotal($(this).closest('.material_popup_wrapper'));
            calculatePopupSubtotal($(this).closest('.material_popup_wrapper'));

        });

        $(this).find('#unitType').on('change keydown paste input', function(){
            let row = $(this).closest('ul').data('row');

            material_collection.splice(row, row, material_item_obj);
            material_collection[row].setType($(this).val())
        });

        $(this).find('.price-input').on('change keydown paste input', function(){
            let row = $(this).closest('ul').data('row');
            material_collection.splice(row, row, material_item_obj);
            material_collection[row].setPrice($(this).val())
            material_collection[row].calculateTotal();
            calculatePopupItemTotal($(this).closest('ul'));
            calculatePopupTotal($(this).closest('.material_popup_wrapper'));
            calculatePopupSubtotal($(this).closest('.material_popup_wrapper'));
        });

        $(this).find('.percent-input').on('change keydown paste input', function () {
            let row = $(this).closest('ul').data('row');
            material_collection.splice(row, row, material_item_obj);
            material_collection[row].setMarkup($(this).val())
            material_collection[row].calculateTotal();
            calculatePopupItemTotal($(this).closest('ul'));
            calculatePopupTotal($(this).closest('.material_popup_wrapper'));
            calculatePopupSubtotal($(this).closest('.material_popup_wrapper'));
        })

        //delete material row
        $(this).find('.m-table__cell--actions').click(function(){
            let row = $(this).closest('ul').data('row');
            delete material_collection[row];
            material_collection = material_collection.filter(x => x !== null);
           $(this).closest('.m-table__row').remove();
        });
    });

    return material_collection;
}

function initEstimateSectionEvents(section) {

    //delete first item which was created
    $(section).find('.remove-item').click(function(){
        let parent = $(this).closest('.pwrk-frm__item');
        let parent_row = $(parent).data('row');
        $(parent).remove();
        delete item_collection[parent_row];
    });

    $(section).find("input[name='title']").change(function () {
        let title = $(this).val();
        let parent = $(this).closest('.pwrk-frm__item');
        let parent_row = $(parent).data('row');
        let quote_object = item_collection[parent_row];

        quote_object.setTitle(title);
    });

    //TODO: connect ckeditor for getting value
    $(section).find("input[name='description']").change(function () {
        console.log($(this).val())
        let description = $(this).val();
        let parent = $(this).closest('.pwrk-frm__item');
        let parent_row = $(parent).data('row');
        let quote_object = item_collection[parent_row];
        quote_object.setDescription(description);
    });
    //on change material amount

    //on change price, set it to the object
    $(section).find("input[name='price']").on('change keydown paste input', function () {

        let price = parseFloat($(this).val());
        let parent = $(this).closest('.pwrk-frm__item');
        let total = $(parent).find('.total')[0];
        let subtotal = $(parent).find('.subtotal')[0];
        let parent_row = $(parent).data('row');
        let quote_object = item_collection[parent_row];
        quote_object.setPrice(price);
        quote_object.calculateSubTotal();
        quote_object.calculateTotal();
        $(subtotal).text('£' + quote_object.subtotal);
        $(total).text('£' + quote_object.total);
        calculateEstimateItemSubtotal(parent)
        calculateEstimateItemTotal(parent)
    });

    //on change taxRate, set it to the object
    $(section).find("select[name='taxRate']").on('change', function () {
        let tax_rate = +$(this).val();
        let parent = $(this).closest('.pwrk-frm__item');
        let total = $(parent).find('.total')[0];
        let parent_row = $(parent).data('row');
        let quote_object = item_collection[parent_row];
        quote_object.setTaxRate(tax_rate);
        quote_object.calculateSubTotal();
        quote_object.calculateTotal();
        $(total).text('£' + quote_object.total);
        calculateEstimateItemSubtotal(parent)
        calculateEstimateItemTotal(parent)
    });
    // end changing inputs

    //show materials popup
    $(section).find('.material_popup').click(function () {
        let parent = $(this).closest('.pwrk-frm__item');

        let parent_row = $(parent).data('row');
        let quote_object = item_collection[parent_row];
        let material_popup = quote_object.getMaterialTemplate(parent_row, quote_object.materials.length++);
        let parsed_popup = $.parseHTML(material_popup);
        calculatePopupTotal(parsed_popup[1]);
        calculatePopupSubtotal(parsed_popup[1]);
        let delete_row = $(parsed_popup).find('.m-table__cell--actions');
        let material_collection = [];

        calculatePopupTotal(parsed_popup[1])
        calculatePopupSubtotal(parsed_popup[1])
        //delete rows rendered with template
        $(delete_row).click(function(){
            let row = $(this).closest('ul').data('row');
            delete material_collection[row];
            $(this).closest('.m-table__row').remove()
        });

        $('body').append(parsed_popup);
        let close_btn = $(parsed_popup).find('.close_material_popup')[0];

        let material_item_obj = new MaterialItem();

        // change title
        $(parsed_popup).find('.material_title').change(function(){
            let title = $(this).val();
            material_item_obj.setTitle(title)
        });

        // change type
        $(parsed_popup).find('.m_type').on('change keydown paste input', function () {
            material_item_obj.setType($(this).val())
        });

        // change qty
        $(parsed_popup).find('.material_qty').on('change keydown paste input', function(){
            material_item_obj.setQty($(this).val())
            material_item_obj.calculateTotal();
            const row_item = $(this).parent().parent();
            calculatePopupItemTotal(row_item)
            calculatePopupTotal(parsed_popup[1]);
            calculatePopupSubtotal(parsed_popup[1]);
        });

        // change price
        $(parsed_popup).find('.price-input').on('change keydown paste input', function () {
            material_item_obj.setPrice($(this).val())
            material_item_obj.calculateTotal();
            const row_item = $(this).parent().parent().parent();
            calculatePopupItemTotal(row_item)
            calculatePopupTotal(parsed_popup[1]);
            calculatePopupSubtotal(parsed_popup[1]);
        });

        // change markup
        $(parsed_popup).find('.percent-input').on('change keydown paste input', function(){
            material_item_obj.setMarkup($(this).val())
            material_item_obj.calculateTotal();
            const row_item = $(this).parent().parent().parent();
            calculatePopupItemTotal(row_item);
            calculatePopupTotal(parsed_popup[1]);
            calculatePopupSubtotal(parsed_popup[1]);
        });
        //push to the collection first material item comes from template
        material_collection.push(material_item_obj);

        //add new material item
        let row_increment = 0;
        $(parsed_popup).find('.add_material_button').click(function(){
            row_increment++;
            let parent = $(this).parent().siblings('.m-table').children('div');
            let row = MaterialItem.getMaterialRow(row_increment);
            $(parent).append(row);
            let row_id = '#' + $(row).attr('id');
            addEvent(row_id, material_collection, material_item_obj);
        });

        //end logic for material

        $(close_btn).click(function(){
            let object_id = $(this).parent().data('object_id');

            const rows = $(parsed_popup).find('ul');

            material_collection = [];

            rows.each(function() {
                let item = new LabourItem();
                item.setTitle($(this).find('.material_title').val())
                item.setMarkup($(this).find('.percent-input').val());
                item.setPrice($(this).find('.price-input').val());
                item.setQty($(this).find('.material_qty').val());
                item.setType($(this).find('.m_type').val());
                item.calculateTotal();
                material_collection.push(item);
            })

            item_collection[object_id].setMaterials(material_collection);
            item_collection[object_id].calculateMaterialAmount();
            item_collection[object_id].calculateSubTotal();
            item_collection[object_id].calculateTotal();

            $(parent).find('.material_input').val(item_collection[object_id].material_amount);
            $(this).parent().parent().remove();

            calculateEstimateItemSubtotal(parent);
            calculateEstimateItemTotal(parent);
        })

    });
    //end show materials popup


    /**
     * PASHA
     */
    $(section).find('.labour_popup').click(function () {
        let parent = $(this).closest('.pwrk-frm__item');
        //parent_row = object id
        let parent_row = $(parent).data('row');
        let quote_object = item_collection[parent_row];
        let labour_popup = quote_object.getLabourTemplate(parent_row, quote_object.labours.length++);
        let parsed_popup = $.parseHTML(labour_popup);
        calculatePopupTotal(parsed_popup[1])
        calculatePopupSubtotal(parsed_popup[1])
        let delete_row = $(parsed_popup).find('.m-table__cell--actions');
        let labour_collection = [];
        //delete rows rendered with template
        $(delete_row).click(function(){
            let row = $(this).closest('ul').data('row');
            delete labour_collection[row];
            $(this).closest('.m-table__row').remove()
        });

        $('body').append(parsed_popup);
        let close_btn = $(parsed_popup).find('.close_material_popup')[0];

        let labour_item_obj = new LabourItem();
        // change title
        $(parsed_popup).find('.material_title').change(function(){
            let title = $(this).val();
            labour_item_obj.setTitle(title)
        });

        // change type
        $(parsed_popup).find('.m_type').on('change keydown paste input', function () {
            labour_item_obj.setType($(this).val())
        });

        // change qty
        $(parsed_popup).find('.material_qty').on('change keydown paste input', function(){
            labour_item_obj.setQty($(this).val())
            labour_item_obj.calculateTotal();
            const row_item = $(this).parent().parent();
            calculatePopupItemTotal(row_item);
            calculatePopupTotal(parsed_popup[1]);
            calculatePopupSubtotal(parsed_popup[1]);
        });

        // change price
        $(parsed_popup).find('.price-input').on('change keydown paste input', function () {
            labour_item_obj.setPrice($(this).val())
            labour_item_obj.calculateTotal();
            const row_item = $(this).parent().parent().parent();
            calculatePopupItemTotal(row_item);
            calculatePopupTotal(parsed_popup[1]);
            calculatePopupSubtotal(parsed_popup[1]);
        });

        // change markup
        $(parsed_popup).find('.percent-input').on('change keydown paste input', function(){
            labour_item_obj.setMarkup($(this).val())
            labour_item_obj.calculateTotal();
            const row_item = $(this).parent().parent().parent();
            calculatePopupItemTotal(row_item);
            calculatePopupTotal(parsed_popup[1]);
            calculatePopupSubtotal(parsed_popup[1]);
        });
        //push to the collection first material item comes from template
        labour_collection.push(labour_item_obj);

        //add new material item
        let row_increment = quote_object.labours.length - 1;
        $(parsed_popup).find('.add_material_button').click(function(){
            row_increment++;
            let parent = $(this).parent().siblings('.m-table').children('div');
            // let material_item  = new MaterialItem();
            // $(parent).append($(parsed_popup).find('.m-table__row--empty').first().clone(true, true))
            let row = LabourItem.getLabourRow(row_increment);
            $(parent).append(row);

            let row_id = '#' + $(row).attr('id');
            let labour_item_obj = new LabourItem();
            addEvent(row_id, labour_collection, labour_item_obj);
        });


        $(close_btn).click(function(){
            let object_id = $(this).parent().data('object_id');

            const rows = $(parsed_popup).find('ul');

            labour_collection = [];

            rows.each(function() {
                let item = new LabourItem();
                item.setTitle($(this).find('.material_title').val())
                item.setMarkup($(this).find('.percent-input').val());
                item.setPrice($(this).find('.price-input').val());
                item.setQty($(this).find('.material_qty').val());
                item.setType($(this).find('.m_type').val());
                item.calculateTotal();
                labour_collection.push(item);
            })

            item_collection[object_id].setLabours(labour_collection);
            item_collection[object_id].calculateLaboursAmount();
            item_collection[object_id].calculateSubTotal();
            item_collection[object_id].calculateTotal();

            $(parent).find('.labour_popup input').val(item_collection[object_id].labours_amount);
            $(this).parent().parent().remove();

            calculateEstimateItemSubtotal(parent);
            calculateEstimateItemTotal(parent);
        })
    });
    /**
     * PASHA END
     */

    //delete item
    $(section).find('.remove-item').click(function(){
        let parent = $(this).closest('.pwrk-frm__item');
        let parent_row = $(parent).data('row');
        $(parent).remove();
        delete item_collection[parent_row];
    });
}

function calculatePopupTotal(html_popup) {
    let total = 0;

    $(html_popup).find('ul').each(function () {
        const price = $(this).find('.price-input').val();
        const qty = $(this).find('.material_qty').val();
        const percent = $(this).find('.percent-input').val();

        total += price * qty + (price * qty * (percent / 100));
    });

    if (total < 0 || isNaN(total)) {
        total = 0;
    }

    $(html_popup).find('span[data-test="material-items-total"]').text(`£${parseFloat(total)}`)}

function calculatePopupSubtotal(html_popup) {
    let total = 0;

    $(html_popup).find('ul').each(function () {
        const price = $(this).find('.price-input').val();
        const qty = $(this).find('.material_qty').val();

        total += price * qty;
    });

    if (total < 0 || isNaN(total)) {
        total = 0;
    }

    $(html_popup).find('span[data-test="material-items-subtotal"]').text(`£${parseFloat(total)} before markup`)
}

function calculateEstimateItemSubtotal(estimateItemHtml) {
    let mat_price = parseFloat($(estimateItemHtml).find('.material_popup').find('input').val());
    let lab_price = parseFloat($(estimateItemHtml).find('.labour_popup').find('input').val());
    let price = parseFloat($(estimateItemHtml).find('input[name="price"]').val());

    let total = mat_price + lab_price + price;

    if (total < 0 || isNaN(total)) {
        total = 0;
    } else {
        total = new Intl.NumberFormat().format(total)
    }

    $(estimateItemHtml).find('span.subtotal').text(`£${total}`)

    calculateGlobalSubtotal();
}

function calculateEstimateItemTotal(estimateItemHtml) {
    let mat_price = parseFloat($(estimateItemHtml).find('.material_popup').find('input').val());
    let lab_price = parseFloat($(estimateItemHtml).find('.labour_popup').find('input').val());
    let price = parseFloat($(estimateItemHtml).find('input[name="price"]').val());
    let percent = parseFloat($(estimateItemHtml).find('select[name="taxRate"] option:selected').val());

    let total = mat_price + lab_price + price + ( (mat_price + lab_price + price) * percent / 100);

    if (total < 0 || isNaN(total)) {
        total = 0;
    } else {
        total = new Intl.NumberFormat().format(total)
    }

    $(estimateItemHtml).find('span.total').text(`£${total}`)

    calculateGlobalTotal();
}

function calculatePopupItemTotal(item) {
    let qty = $(item).find('.material_qty').val();
    let price = $(item).find('.price-input').val();
    let percent = $(item).find('.percent-input').val();

    if (qty < 0 || isNaN(qty)) {
        qty = 1;
    }

    if (price < 0 || isNaN(price)) {
        price = 0;
    }

    if (percent < 0 || isNaN(percent)) {
        percent = 0;
    }

    let total = (qty * price) + (qty * price * percent / 100);

    if (total < 0 || isNaN(total)) {
        total = 0;
    } else {
        total = new Intl.NumberFormat().format(total)
    }

    $(item).find('.m-table__input--total').text(`£${total}`)
}

function calculateGlobalTotal() {
    const items = $('.pwrk-frm__item');

    let total = 0;

    items.each((index, item) => {
        let mat_price = parseFloat($(item).find('.material_popup').find('input').val());
        let lab_price = parseFloat($(item).find('.labour_popup').find('input').val());
        let price = parseFloat($(item).find('input[name="price"]').val());
        let percent = parseFloat($(item).find('select[name="taxRate"] option:selected').val());

        let result = mat_price + lab_price + price + ( (mat_price + lab_price + price) * percent / 100);

        if (result < 0 || isNaN(result)) {
            result = 0;
        }

        total += result
    })

    $('span.pwrk-frm__totals-value[data-test="total"]').text(`£${new Intl.NumberFormat().format(total)}`);
}

function calculateGlobalSubtotal() {
    const items = $('.pwrk-frm__item');

    let subtotal = 0;

    items.each((index, item) => {
        let mat_price = parseFloat($(item).find('.material_popup').find('input').val());
        let lab_price = parseFloat($(item).find('.labour_popup').find('input').val());
        let price = parseFloat($(item).find('input[name="price"]').val());
        let result = mat_price + lab_price + price;

        if (result < 0 || isNaN(result)) {
            result = 0;
        }

        subtotal += result
    })

    $('span.pwrk-frm__totals-value[data-test="sub-total"]').text(`£${new Intl.NumberFormat().format(subtotal)}`);
}

$('.pwrk-frm__save-submit').click(function(){
    let document_title = $('#documentTitle').val();
    let reference = $('#reference').val();
    let customerName = $('#customerName').val();
    let customerAddress = $('#customerAddress').val();
    let customerPostcode = $('#customerPostcode').val();
    let jobAddress = $('#jobAddress').val();
    let jobPostcode = $('#jobPostcode').val();
    let introduction_trix = $('#introduction_trix').val();
    let notes_trix = $('#notes_trix').val();
    let terms_trix = $('#terms_trix').val();

    console.log('submit')
    console.log(item_collection)
    console.log('submit')
});
//add event and calculate material costs per item
function addEvent (id, material_collection, material_item_obj, event = 'click', quote_object = null) {
    $(id)[0].addEventListener(event, function() {

        $(this).find('.material_title').change(function () {
            let row = $(this).closest('ul').data('row');
            material_collection.splice(row, row, material_item_obj);
            material_collection[row].setTitle($(this).val());
        });

        $(this).find('.material_qty').on('change keydown paste input', function(){
            let total_per_item = $(this).parent().parent().find('.m-table__input--total');
            let row = $(this).closest('ul').data('row');
            material_collection.splice(row, row, material_item_obj);
            material_collection[row].setQty($(this).val());
            material_collection[row].calculateTotal();
            $(total_per_item).text(material_item_obj.total);
            calculatePopupTotal($(this).closest('.material_popup_wrapper'));
            calculatePopupSubtotal($(this).closest('.material_popup_wrapper'));
        });

        $(this).find('#unitType').on('change keydown paste input', function(){
            let row = $(this).closest('ul').data('row');

            material_collection.splice(row, row, material_item_obj);
            material_collection[row].setType($(this).val())
        });

        $(this).find('#itemPrice').on('change keydown paste input', function(){
            let row = $(this).closest('ul').data('row');
            let total_per_item = $(this).parent().parent().parent().find('.m-table__input--total');
            material_collection.splice(row, row, material_item_obj);
            material_collection[row].setPrice($(this).val())
            material_collection[row].calculateTotal();
            $(total_per_item).text(material_item_obj.total);
            calculatePopupTotal($(this).closest('.material_popup_wrapper'));
            calculatePopupSubtotal($(this).closest('.material_popup_wrapper'));
        });

        $(this).find('#markupPercentage').on('change keydown paste input', function () {
            let row = $(this).closest('ul').data('row');
            let total_per_item = $(this).parent().parent().parent().find('.m-table__input--total');
            material_collection.splice(row, row, material_item_obj);
            material_collection[row].setMarkup($(this).val())
            material_collection[row].calculateTotal();
            $(total_per_item).text(material_item_obj.total);
            calculatePopupTotal($(this).closest('.material_popup_wrapper'));
            calculatePopupSubtotal($(this).closest('.material_popup_wrapper'));
        })
        console.log('dynamic material collection')
        console.log(material_collection);
        console.log('dynamic material collection')

        //delete material row
        $(this).find('.m-table__cell--actions').click(function(){
            let row = $(this).closest('ul').data('row');
            console.log('before')
            console.log(material_collection);
            delete material_collection[row];
            material_collection = material_collection.filter(x => x !== null);
           $(this).closest('.m-table__row').remove();
           console.log('after');
           console.log(material_collection);
        });
    });

    return material_collection;
};

//calculate subtotal and total for materials per object
//delete first item which was created
$('.remove-item').click(function(){
    let parent = $(this).closest('.pwrk-frm__item');
    let parent_row = $(parent).data('row');
    $(parent).remove();
    delete item_collection[parent_row];
});

$('.parent_section').each(function(item){
    console.log($(item).find('.subtotal'))
});
