class LabourItem{
    constructor(id, title, qty = 1, type, price = 0, markup = 0, total = 0){
        this.title = title;
        this.qty = qty;
        this.type = type;
        this.price = price;
        this.markup = markup;
        this.total = total;
        this.id = id;
    }

    setId(id){
        this.id = parseInt(id);
    }

    static getLabourRow(row){
        const select = `
                <select name="unitType" autocomplete="off" inputmode="" class="pwrk-frm__input m-table__input m_type" data-test="unit-type-field" id="unitType">
                    <option value="test">Hours</option>
                    <option value="test2">Days</option>
                    <option value="test2">Sq. Mtr.</option>
                    <option value="test2">Meter</option>
                    <option value="test2">Sq. Ft.</option>
                    <option value="test2">Ft</option>
                    <option value="test2">Units</option>
                    <option value="test2">Each</option>
                    <option value="test2">Minutes</option>
                 </select
                `;

        return '<ul data-row="' + row + '" id="row-' + row +'" class="m-table__row m-table__row--empty" data-test="material-item">\n' +
            '   <li data-label="Material" class="m-table__cell m-table__cell--title">\n' +
            '      <a class="m-table__row-drag-handle">\n' +
            '         <span class="icon icon--tiny">\n' +
            '            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">\n' +
            '               <path d="M12 23.998a.755.755 0 0 1-.26-.047l-.022-.008a.732.732 0 0 1-.249-.165l-3.749-3.75a.744.744 0 0 1 0-1.06.744.744 0 0 1 1.06 0l2.47 2.47V2.559l-2.47 2.47a.744.744 0 0 1-1.06-.001c-.142-.141-.22-.33-.22-.53s.078-.389.22-.53L11.469.219a.74.74 0 0 1 .245-.163l.025-.009a.733.733 0 0 1 .521.001l.021.007c.097.04.179.095.25.166L16.28 3.97c.142.141.22.33.22.53s-.078.389-.22.53a.749.749 0 0 1-1.06 0l-2.47-2.47v18.879l2.47-2.47a.744.744 0 0 1 1.06 0 .749.749 0 0 1 0 1.06l-3.75 3.75a.763.763 0 0 1-.246.164l-.027.01a.769.769 0 0 1-.257.045z"></path>\n' +
            '            </svg>\n' +
            '         </span>\n' +
            '      </a>\n' +
            '      <input name="title" placeholder="Enter material description" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m-table__input material_title m-table__input--title" data-test="description-field" id="description" value="">\n' +
            '      <div></div>\n' +
            '   </li>\n' +
            '   <li data-label="Qty" class="m-table__cell m-table__cell--qty"><input name="quantity" placeholder="0" autocomplete="off" inputmode="" maxlength="" class="material_qty pwrk-frm__input m-table__input m-table__input--qty" data-test="quantity-field" id="quantity" value="1"></li>\n' +
            '   <li data-label="Unit type" class="m-table__cell m-table__cell--units">' + select +
            '</li>\n' +
            '   <li data-label="£ per unit" class="m-table__cell m-table__cell--price"><span class="input--currency" data-currency-unit="£"><input name="itemPrice" type="number" placeholder="0.00" autocomplete="off" inputmode="decimal" maxlength="12" class="pwrk-frm__input m-table__input m-table__input--price price-input" data-test="item-price-field" id="itemPrice" value="0.00"></span></li>\n' +
            '   <li data-label="% markup" class="m-table__cell m-table__cell--markup"><span class="input--percentage"><input name="markupPercentage" placeholder="0" autocomplete="off" inputmode="" maxlength="" class="pwrk-frm__input m-table__input m-table__input--markup percent-input" data-test="markup-percentage-field" id="markupPercentage" value="0.0"></span></li>\n' +
            '   <li data-label="Total" class="m-table__cell m-table__cell--total"><span class="m-table__input--total" data-test="material-item-total">£0.00</span></li>\n' +
            '   <li class="m-table__cell m-table__cell--actions">\n' +
            '      <button data-test="remove-material-item-button" class="btn btn--tiny btn--icon-only" type="button">\n' +
            '         <span class="icon icon--tiny">\n' +
            '            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">\n' +
            '               <path d="M6.631 23.25a2.263 2.263 0 0 1-2.242-2.064L3.06 5.25H1.5a.75.75 0 0 1 0-1.5h6V3A2.252 2.252 0 0 1 9.75.75h4.5A2.252 2.252 0 0 1 16.5 3v.75h6a.75.75 0 0 1 0 1.5h-1.56l-1.328 15.937a2.262 2.262 0 0 1-2.242 2.063H6.631zm-.748-2.188c.032.386.36.688.748.688H17.37a.753.753 0 0 0 .747-.688L19.435 5.25H4.565l1.318 15.812zM15 3.75V3a.75.75 0 0 0-.75-.75h-4.5A.75.75 0 0 0 9 3v.75h6z"></path>\n' +
            '               <path d="M9.75 18a.75.75 0 0 1-.75-.75v-7.5a.75.75 0 0 1 1.5 0v7.5a.75.75 0 0 1-.75.75zm4.5 0a.75.75 0 0 1-.75-.75v-7.5a.75.75 0 0 1 1.5 0v7.5a.75.75 0 0 1-.75.75z"></path>\n' +
            '            </svg>\n' +
            '         </span>\n' +
            '      </button>\n' +
            '   </li>\n' +
            '</ul>'
    }

    setTitle(title){
        this.title = title;
    }

    setQty(qty){
        if (isNaN(qty)) {
            qty = 1;
        }

        this.qty = parseInt(qty);
    }

    setType(type){
        this.type = type;
    }

    setPrice(price){
        if (isNaN(price)) {
            price = 0;
        }

        this.price = parseFloat(price);
    }

    setMarkup(markup){
        this.markup = markup
    }

    calculateTotal(){
        this.subtotal = this.price * this.qty;

        if (this.subtotal < 0 || isNaN(this.subtotal)) {
            this.subtotal = 0;
        }

        if(this.markup === 0){
            this.total = this.subtotal
            return
        }

        this.total = this.subtotal * this.markup / 100 + this.subtotal;

        if (this.total < 0 || isNaN(this.total)) {
            this.total = 0;
        }
    }
}
